package invalidation;

import validation.repository.Validation;

import java.util.List;

/**
 * Interface for all Validation invalidator
 *
 * @author David
 */
public interface Invalidator {

    /**
     * Invalidate and get all the invalidated validation requests
     *
     * @param difference The time before the validation is expired
     * @return A list of all invalidated validations
     */
    List<Validation> getInvalidated(float difference);
}
