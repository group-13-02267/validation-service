package invalidation;

import validation.repository.Validation;
import validation.repository.ValidationRepository;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * Validation invalidator
 *
 * @author David
 */
public class DTUInvalidator implements Invalidator {

    /**
     * Repository containing all validations
     */
    private final ValidationRepository repository;

    /**
     * Instantiate validation invalidator
     * @param repository The repository used to store the validations
     */
    public DTUInvalidator(ValidationRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Validation> getInvalidated(float difference) {
        List<Validation> validations = repository.getAllValidations();

        List<Validation> invalidated = new ArrayList<>();

        for (Validation val : validations) {
            LocalTime from = LocalTime.now();
            LocalTime to = val.getCreation();
            float diff = Math.abs(from.until(to, ChronoUnit.SECONDS));

            if (diff >= difference) {
                invalidated.add(val);
                this.repository.deleteValidation(val.getId());
            }
        }

        return invalidated;
    }

}
