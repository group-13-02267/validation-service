package invalidation;

import converters.PaymentConverter;
import messaging.MessageSender;
import validation.repository.Validation;

import java.util.List;
import java.util.TimerTask;

/**
 * Timer task for invalidating validations recurrently
 *
 * @author David
 */
public class InvalidatorTimerTask extends TimerTask {

    /**
     * MessageSender for sending events
     */
    private final MessageSender sender;

    /**
     * Invalidator for invalidating validations
     */
    private final Invalidator invalidator;

    /**
     * The expire time difference used to expire validations
     */
    private final float difference;

    /**
     * Instantiate a new InvalidatorTimerTask to recurrently invalidate and broadcast expired validations
     *
     * @param sender      The message sender used to broadcast events
     * @param invalidator The invalidator used to invalidate validations
     */
    public InvalidatorTimerTask(MessageSender sender, Invalidator invalidator) {
        this.sender = sender;
        this.invalidator = invalidator;
        difference = 15;
    }

    /**
     * Instantiate a new InvalidatorTimerTask to recurrently invalidate and broadcast expired validations
     *
     * @param sender      The message sender used to broadcast events
     * @param invalidator The invalidator used to invalidate validations
     * @param difference  The difference in time before a validation is expired
     */
    public InvalidatorTimerTask(MessageSender sender, Invalidator invalidator, float difference) {
        this.sender = sender;
        this.invalidator = invalidator;
        this.difference = difference;
    }

    /**
     * Run the invalidator task
     */
    @Override
    public void run() {
        List<Validation> invalidated = invalidator.getInvalidated(difference);
        for (Validation inva : invalidated) {
            boolean isRefund = inva.getType().toLowerCase().equals("refund");
            sender.sendMessage(PaymentConverter.paymentToResponseDTO(inva.getPayment(), false, isRefund),
                    "transaction." + inva.getType() + ".handled." + inva.getId());
        }
    }
}
