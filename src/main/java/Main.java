import DTO.PaymentDTO;
import DTO.TokenValidationDTO;
import DTO.ValidationDTO;
import core.AsyncException;
import core.AsyncService;
import core.Microservice;
import invalidation.DTUInvalidator;
import invalidation.Invalidator;
import invalidation.InvalidatorTimerTask;
import messaging.MessageSender;
import messaging.RabbitMqMessageSenderService;
import validation.receivers.MerchantValidateService;
import validation.receivers.PaymentValidatorService;
import validation.receivers.RefundValidatorService;
import validation.receivers.TokenValidateService;
import validation.repository.DTUValidationRepository;
import validation.repository.ValidationRepository;

import java.util.Timer;


public class Main extends Microservice {

    public static void main(String[] args) throws Exception {
        new Main().startup();
    }

    @Override
    public void startup() throws AsyncException {
        MessageSender sender = new RabbitMqMessageSenderService();
        ValidationRepository repository = new DTUValidationRepository();
        Invalidator invalidator = new DTUInvalidator(repository);
        AsyncService<PaymentDTO> paymentValidationService = new PaymentValidatorService(repository, sender);
        AsyncService<PaymentDTO> refundValidationService = new RefundValidatorService(repository, sender);
        AsyncService<ValidationDTO> merchantPaymentValidateService = new MerchantValidateService(repository, sender);
        AsyncService<TokenValidationDTO> tokenPaymentValidateService = new TokenValidateService(repository, sender);
        paymentValidationService.listen();
        refundValidationService.listen();
        merchantPaymentValidateService.listen();
        tokenPaymentValidateService.listen();
        Timer invalidationTimer = new Timer();
        invalidationTimer.scheduleAtFixedRate(new InvalidatorTimerTask(sender, invalidator), 5, 5);
    }
}
