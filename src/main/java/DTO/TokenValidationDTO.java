package DTO;

/**
 * DTO for validating tokens
 */
public class TokenValidationDTO extends ValidationDTO {
    /**
     * The customer that the token belongs to
     */
    public String customer;
}
