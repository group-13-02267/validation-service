package DTO;

/**
 * Base class DTO for validating merchant and tokens
 */
public class ValidationDTO {

    /**
     * The unique request id
     */
    public String id;

    /**
     * The validation status
     */
    public boolean status;
}
