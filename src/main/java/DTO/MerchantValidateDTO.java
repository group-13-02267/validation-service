package DTO;

/**
 * DTO for validating merchant
 */
public class MerchantValidateDTO {
    public String id;
    public String merchantId;

    public MerchantValidateDTO() {
    }

    public MerchantValidateDTO(String id, String merchantId) {
        this.id = id;
        this.merchantId = merchantId;
    }

}
