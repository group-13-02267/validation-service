package DTO;

import java.io.Serializable;

/**
 * Payment Response DTO
 *
 * @author Georg
 */
public class PaymentResponseDTO implements Serializable {

    /**
     * Payment validation status
     */
    public boolean status;

    /**
     * Unique payment request id
     */
    public String id;

    /**
     * The payment sender
     */
    public String sender;

    /**
     * The token used to request the payment
     */
    public String token;

    /**
     * The payment receiver
     */
    public String receiver;

    /**
     * The payment amount
     */
    public float amount;

    /**
     * Is this payment a refund or a payment
     */
    public boolean isRefund;

    /**
     * The payment creation date
     */
    public String createdDate;

}
