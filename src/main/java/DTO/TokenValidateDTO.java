package DTO;

/**
 * DTO to validate token
 */
public class TokenValidateDTO {

    /**
     * The unique token validation request id
     */
    public String id;

    /**
     * The token to validate
     */
    public String token;

    public TokenValidateDTO() {
    }

    public TokenValidateDTO(String id, String token) {
        this.id = id;
        this.token = token;
    }

}
