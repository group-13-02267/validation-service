package DTO;

/**
 * Payment Request DTO
 *
 * @author David
 */
public class PaymentDTO {

    /**
     * Unique payment request id
     */
    public String id;

    /**
     * The token used to request the payment
     */
    public String token;

    /**
     * The payment receiver
     */
    public String receiver;

    /**
     * The payment sender
     */
    public String sender;

    /**
     * The payment amount
     */
    public float amount;
}
