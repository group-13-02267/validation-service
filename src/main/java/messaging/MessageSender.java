package messaging;

/**
 * Interface for sending and broadcasting messages and events
 *
 * @author Mathias
 */
public interface MessageSender {
    void sendMessage(Object content, String route);
}
