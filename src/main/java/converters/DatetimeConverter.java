package converters;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

import static java.time.temporal.ChronoField.*;

/**
 * @author David
 */
public class DatetimeConverter {
    /**
     * Set the DateTimeFormatter to accept dd-mm-yyyy
     */
    private static DateTimeFormatter formatter = new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .appendValue(DAY_OF_MONTH, 2)
            .appendLiteral('-')
            .appendValue(MONTH_OF_YEAR, 2)
            .appendLiteral('-')
            .appendValue(YEAR, 4)
            .toFormatter();

    /**
     * Convert a LocalDate to a String
     *
     * @param date The date to convert
     * @return The date as String with dd-mm-yyyy format
     */
    public static String convertDateToString(LocalDate date) {
        return date.format(formatter);
    }

}
