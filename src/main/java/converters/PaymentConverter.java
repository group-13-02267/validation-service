package converters;

import DTO.PaymentDTO;
import DTO.PaymentResponseDTO;

import java.time.LocalDate;

/**
 * Payment DTO converters
 *
 * @author David
 */
public class PaymentConverter {

    private PaymentConverter() {
    }

    /**
     * Converting a PaymentDTO to a PaymentResponse DTO
     * @param payment the payment to convert
     * @param status the payment validation status
     * @param isRefund Is it a refund
     * @return The PaymentResponseDTO from the payment with the createdDate set to now
     */
    public static PaymentResponseDTO paymentToResponseDTO(PaymentDTO payment, boolean status, boolean isRefund) {
        PaymentResponseDTO response = new PaymentResponseDTO();
        response.id = payment.id;
        response.sender = payment.sender;
        response.token = payment.token;
        response.receiver = payment.receiver;
        response.amount = payment.amount;
        response.status = status;
        response.isRefund = isRefund;
        response.createdDate = DatetimeConverter.convertDateToString(LocalDate.now());
        return response;
    }

}
