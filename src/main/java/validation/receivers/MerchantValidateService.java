package validation.receivers;

import DTO.PaymentDTO;
import DTO.ValidationDTO;
import com.google.gson.Gson;
import messaging.MessageSender;
import validation.repository.ValidationRepository;

/**
 * Service for receiving the merchant validation result
 *
 * @author David
 */
public class MerchantValidateService extends SingleValidationService<ValidationDTO> {

    public MerchantValidateService(ValidationRepository repository, MessageSender sender) {
        super("merchant.validate.*", repository, sender);
    }

    @Override
    protected ValidationDTO handleMessage(String message) {
        return new Gson().fromJson(message, ValidationDTO.class);
    }


    @Override
    public void handleReceive(ValidationDTO dto, PaymentDTO validation, String type) {

    }

    @Override
    public String getField() {
        return "merchant";
    }
}
