package validation.receivers;

import DTO.PaymentDTO;
import DTO.PaymentResponseDTO;
import DTO.ValidationDTO;
import converters.PaymentConverter;
import core.AsyncService;
import messaging.MessageSender;
import validation.repository.Validation;
import validation.repository.ValidationRepository;

import java.security.InvalidKeyException;

/**
 * Abstract class for validating an object asynchronously
 *
 * @author David
 */
public abstract class SingleValidationService<T extends ValidationDTO> extends AsyncService<T> {

    /**
     * The repository that holds the validation requests
     */
    protected final ValidationRepository repository;

    /**
     * The message sender
     */
    protected final MessageSender sender;

    /**
     * Instantiate a SingleValidationService responsible for asynchronously validating objects
     * @param route The route to receive the validation results on
     * @param repository The repository that stores and handles the validation requests
     * @param sender The message sender
     */
    public SingleValidationService(String route, ValidationRepository repository, MessageSender sender) {
        super("payment", route);
        this.repository = repository;
        this.sender = sender;
    }

    @Override
    public void receive(T t) {
        try {
            if (!t.status) {
                prepareAndSendResponse(t, false);
            }
            Validation validation = repository.getValidation(t.id);
            repository.setValidation(t.id, t.status, getField());
            handleReceive(t, validation.getPayment(), validation.getType());
        } catch (InvalidKeyException e) {
            System.out.println("Could not the validation for " + t.id);
        }

        try {
            if (repository.isValid(t.id)) {
                prepareAndSendResponse(t, true);
            }
        } catch (InvalidKeyException e) {
            System.out.println("Could not the validation for " + t.id);
        }
    }

    /**
     * Handle the validation response
     *
     * @param dto        The received request
     * @param validation The payment request
     * @param type       The type of request - either "refund" or "payment"
     */
    public abstract void handleReceive(T dto, PaymentDTO validation, String type);

    /**
     * Get which validation type it is
     *
     * @return The validation type like "token" or "merchant"
     */
    public abstract String getField();

    /**
     * Prepare the response
     *
     * @param request   The received request
     * @param validated The validation status
     * @throws InvalidKeyException Thrown if the validation request does not exist in the repository
     */
    private void prepareAndSendResponse(T request, boolean validated) throws InvalidKeyException {
        Validation validation = repository.getValidation(request.id);

        handleReceive(request, validation.getPayment(), validation.getType());

        boolean isRefund = validation.getType().equals("refund");

        PaymentResponseDTO paymentResponse =
                PaymentConverter.paymentToResponseDTO(validation.getPayment(), validated, isRefund);

        sendResponse(validation, paymentResponse);
    }

    /**
     * Send the response
     *
     * @param validation      The validation request
     * @param paymentResponse The created payment response
     */
    private void sendResponse(Validation validation, PaymentResponseDTO paymentResponse) {
        sender.sendMessage(paymentResponse, "transaction." + validation.getType() + ".validated." + validation.getId());
        repository.deleteValidation(validation.getId());
    }
}
