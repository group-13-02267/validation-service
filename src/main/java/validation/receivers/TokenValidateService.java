package validation.receivers;

import DTO.PaymentDTO;
import DTO.TokenValidationDTO;
import com.google.gson.Gson;
import messaging.MessageSender;
import validation.repository.ValidationRepository;

/**
 * Service for receiving the token validation result
 *
 * @author David
 */
public class TokenValidateService extends SingleValidationService<TokenValidationDTO> {

    public TokenValidateService(ValidationRepository repository, MessageSender sender) {
        super("token.validate.*", repository, sender);
    }

    @Override
    protected TokenValidationDTO handleMessage(String message) {
        return new Gson().fromJson(message, TokenValidationDTO.class);
    }


    @Override
    public void handleReceive(TokenValidationDTO dto, PaymentDTO validation, String type) {
        // Depending on whether it is a refund or payment request,
        // we must enrich the correct receiver and sender
        if (type.equals("refund")) {
            validation.receiver = dto.customer;
        } else {
            validation.sender = dto.customer;
        }
    }

    @Override
    public String getField() {
        return "token";
    }
}
