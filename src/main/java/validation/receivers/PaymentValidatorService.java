package validation.receivers;

import DTO.MerchantValidateDTO;
import DTO.PaymentDTO;
import DTO.TokenValidateDTO;
import com.google.gson.Gson;
import core.AsyncService;
import messaging.MessageSender;
import validation.repository.ValidationRepository;

/**
 * Service for receiving the payment request
 *
 * @author David
 */
public class PaymentValidatorService extends AsyncService<PaymentDTO> {

    private final ValidationRepository repository;

    private final MessageSender sender;

    public PaymentValidatorService(ValidationRepository repository, MessageSender sender) {
        super("payment", "transaction.payment.received.*");
        this.repository = repository;
        this.sender = sender;
    }

    @Override
    protected PaymentDTO handleMessage(String s) {
        return new Gson().fromJson(s, PaymentDTO.class);
    }

    @Override
    public void receive(PaymentDTO paymentDTO) {
        repository.createValidation(paymentDTO.id, "payment", paymentDTO);
        sender.sendMessage(new MerchantValidateDTO(paymentDTO.id, paymentDTO.receiver), "validate.merchant");
        sender.sendMessage(new TokenValidateDTO(paymentDTO.id, paymentDTO.token), "validate.token");
    }

}
