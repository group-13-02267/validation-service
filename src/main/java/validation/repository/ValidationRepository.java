package validation.repository;

import DTO.PaymentDTO;

import java.security.InvalidKeyException;
import java.util.List;

/**
 * Interface for storing ValidationRequests
 *
 * @author David
 */
public interface ValidationRepository {

    /**
     * Create a new validation
     *
     * @param id      The unique request id
     * @param type    The type of validation request fx. "refund" or "payment"
     * @param payment The payment DTO
     */
    void createValidation(String id, String type, PaymentDTO payment);

    /**
     * Set the validation status for a specific field
     *
     * @param id     The validation request id
     * @param status The new status
     * @param field  The field to change
     * @throws InvalidKeyException Thrown if no validation request has the id
     */
    void setValidation(String id, boolean status, String field) throws InvalidKeyException;

    /**
     * Delete a validation
     *
     * @param id The id to delete
     */
    void deleteValidation(String id);

    /**
     * Get all the validations stored
     *
     * @return A list of Validations
     */
    List<Validation> getAllValidations();

    /**
     * Is a stored validation valid
     *
     * @param id The validation request id
     * @return True if valid, false if not
     * @throws InvalidKeyException Thrown if no validation request is stored under that id
     */
    boolean isValid(String id) throws InvalidKeyException;

    /**
     * Get the payment object stored under a validation request
     *
     * @param id The id of the validation request
     * @return The paymentDTO associated with the validation request
     * @throws InvalidKeyException Thrown if no validation request is stored under that id
     */
    PaymentDTO getPayment(String id) throws InvalidKeyException;

    /**
     * Get the validation with a specific id
     *
     * @param id The id of the validation request
     * @return The validation object with the provided id
     * @throws InvalidKeyException Thrown if no validation request is stored under that id
     */
    Validation getValidation(String id) throws InvalidKeyException;

}
