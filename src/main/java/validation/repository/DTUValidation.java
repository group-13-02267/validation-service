package validation.repository;

import DTO.PaymentDTO;

import java.time.LocalTime;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Validation Request Data Holder Object
 *
 * @author David
 */
public class DTUValidation implements Validation {

    /**
     * The unique message id
     */
    private String id;

    /**
     * Whether the token has been verified
     */
    private boolean token = false;

    /**
     * The type of validation request fx. "payment" or "refund"
     */
    private String type;

    /**
     * Whether the merchant has been validated
     */
    private boolean merchant = false;

    /**
     * The payment request DTO
     */
    private PaymentDTO payment;

    /**
     * Time of creation - Used for expiring the validation request
     */
    private LocalTime creation;

    /**
     * Counter to make sure that we are not doing something wrong
     */
    private AtomicInteger count = new AtomicInteger(0);

    /**
     * Validation Request Data Holder Object
     * @param id The unique request id
     * @param type The type of validation request fx. "payment" or "refund"
     * @param paymentDTO The payment DTO
     */
    public DTUValidation(String id, String type, PaymentDTO paymentDTO) {
        this.id = id;
        this.type = type;
        payment = paymentDTO;
        creation = LocalTime.now();
    }

    @Override
    public synchronized boolean isValidated() {
        return token && merchant && count.get() >= 2;
    }

    @Override
    public synchronized void setValidation(boolean status, String field) {
        switch (field.toLowerCase()) {
            case "token":
                token = status;
                count.incrementAndGet();
                break;
            case "merchant":
                merchant = status;
                count.incrementAndGet();
                break;
            default:
                System.out.println("Invalid setValidation field: " + field);
        }
        System.out.println("Setting " + field + " to " + status);
    }

    @Override
    public PaymentDTO getPayment() {
        return payment;
    }

    @Override
    public LocalTime getCreation() {
        return creation;
    }

    public String getId() {
        return id;
    }

    @Override
    public String getType() {
        return type;
    }
}
