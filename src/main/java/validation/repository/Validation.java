package validation.repository;

import DTO.PaymentDTO;

import java.time.LocalTime;

/**
 * Interface for all Validation request objects
 */
public interface Validation {

    /**
     * Is the validation request valid
     *
     * @return True if valid
     */
    boolean isValidated();

    /**
     * Set the validation status of a specific validation field
     *
     * @param status The new status
     * @param field  The field to set the status to
     */
    void setValidation(boolean status, String field);

    /**
     * Get the unique request id
     *
     * @return The unique request id
     */
    String getId();

    /**
     * Get the type of validation request fx. "refund" or "payment"
     *
     * @return The validation request type
     */
    String getType();

    /**
     * Get the payment request DTO
     *
     * @return PaymentDTO
     */
    PaymentDTO getPayment();

    /**
     * Get the time of creation
     *
     * @return The creation time as LocalTime
     */
    LocalTime getCreation();
}
