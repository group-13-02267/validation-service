package validation.repository;

import DTO.PaymentDTO;

import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation for storing Validation requests
 *
 * @author David
 */
public class DTUValidationRepository implements ValidationRepository {

    /**
     * Map that maps from request to Validation object
     */
    private final Map<String, Validation> validations;

    /**
     * Instantiate a new and empty validation repository
     */
    public DTUValidationRepository() {
        validations = new HashMap<>();
    }

    @Override
    public synchronized void createValidation(String id, String type, PaymentDTO payment) {
        validations.put(id, new DTUValidation(id, type, payment));
    }

    @Override
    public synchronized void setValidation(String id, boolean status, String field) throws InvalidKeyException {
        if (!validations.containsKey(id)) throw new InvalidKeyException("Key does not exist: " + id);

        Validation validation = validations.get(id);
        validation.setValidation(status, field);

        validations.put(id, validation);
    }

    @Override
    public synchronized void deleteValidation(String id) {
        if (!validations.containsKey(id)) return;
        validations.remove(id);
    }

    @Override
    public synchronized List<Validation> getAllValidations() {
        List<Validation> validations = new ArrayList<>(this.validations.values());
        return validations;
    }

    @Override
    public synchronized boolean isValid(String id) throws InvalidKeyException {
        if (!validations.containsKey(id)) throw new InvalidKeyException("Key does not exist: " + id);
        Validation validation = validations.get(id);
        return validation.isValidated();
    }

    @Override
    public synchronized PaymentDTO getPayment(String id) throws InvalidKeyException {
        if (!validations.containsKey(id)) throw new InvalidKeyException("Key does not exist: " + id);
        Validation validation = validations.get(id);
        return validation.getPayment();
    }

    @Override
    public Validation getValidation(String id) throws InvalidKeyException {
        if (!validations.containsKey(id)) throw new InvalidKeyException("Key does not exist: " + id);
        return validations.get(id);
    }
}
