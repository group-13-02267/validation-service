package unittests.invalidation;

import DTO.PaymentDTO;
import invalidation.DTUInvalidator;
import invalidation.Invalidator;
import org.junit.Before;
import org.junit.Test;
import validation.repository.DTUValidation;
import validation.repository.Validation;
import validation.repository.ValidationRepository;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DTUInvalidatorTest {

    private ValidationRepository repository;

    private Invalidator invalidator;

    @Before
    public void setup() {
        repository = mock(ValidationRepository.class);
        invalidator = new DTUInvalidator(repository);
    }

    @Test
    public void getInvalidated_whenGettingInvalidated_shouldReturnEmptyList() {
        when(repository.getAllValidations()).thenReturn(getAllValidation());
        List<Validation> invalidated = invalidator.getInvalidated(15);
        assertTrue(invalidated.isEmpty());
    }

    @Test
    public void getInvalidated_whenGettingInvalidated_shouldReturnNoneEmptyList() {
        when(repository.getAllValidations()).thenReturn(getAllLateValidation());
        List<Validation> invalidated = invalidator.getInvalidated(15);
        assertFalse(invalidated.isEmpty());
    }

    private List<Validation> getAllValidation() {
        List<Validation> validations = new ArrayList<>();
        validations.add(generateNowValidation());
        return validations;
    }

    private List<Validation> getAllLateValidation() {
        List<Validation> validations = new ArrayList<>();
        validations.add(generateLateValidation());
        return validations;
    }

    private Validation generateNowValidation() {
        PaymentDTO payment = new PaymentDTO();
        return new DTUValidation(UUID.randomUUID().toString(), "payment", payment);
    }

    private Validation generateLateValidation() {
        return new Validation() {

            @Override
            public boolean isValidated() {
                return false;
            }

            @Override
            public void setValidation(boolean status, String field) {

            }

            @Override
            public String getId() {
                return UUID.randomUUID().toString();
            }

            @Override
            public String getType() {
                return "payment";
            }

            @Override
            public PaymentDTO getPayment() {
                return new PaymentDTO();
            }

            @Override
            public LocalTime getCreation() {
                return LocalTime.now().plusMinutes(1);
            }
        };
    }


}
