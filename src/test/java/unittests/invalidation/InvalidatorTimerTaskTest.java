package unittests.invalidation;

import DTO.PaymentDTO;
import DTO.PaymentResponseDTO;
import invalidation.Invalidator;
import invalidation.InvalidatorTimerTask;
import messaging.MessageSender;
import org.junit.Before;
import org.junit.Test;
import validation.repository.Validation;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;
import java.util.UUID;

import static org.mockito.Mockito.*;

public class InvalidatorTimerTaskTest {

    private MessageSender sender;

    private Invalidator invalidator;

    private TimerTask task;

    @Before
    public void setup() {
        sender = mock(MessageSender.class);
        invalidator = mock(Invalidator.class);
        task = new InvalidatorTimerTask(sender, invalidator);
    }

    @Test
    public void run_whenGettingInvalidatedWith15SecondDifference_shouldSendMessages() {
        when(invalidator.getInvalidated(15)).thenReturn(getAllLateValidation());
        task.run();
        verify(sender, times(2)).sendMessage(any(PaymentResponseDTO.class), startsWith("transaction.payment.handled."));
    }

    @Test
    public void run_whenGettingNoneEmpty_shouldNotSendMessages() {
        when(invalidator.getInvalidated(15)).thenReturn(new ArrayList<>(0));
        task.run();
        verify(sender, never()).sendMessage(any(PaymentResponseDTO.class), startsWith("transaction.payment.handled."));
    }

    private List<Validation> getAllLateValidation() {
        List<Validation> validations = new ArrayList<>();
        validations.add(generateLateValidation());
        validations.add(generateLateValidation());
        return validations;
    }

    private Validation generateLateValidation() {
        return new Validation() {

            @Override
            public boolean isValidated() {
                return false;
            }

            @Override
            public void setValidation(boolean status, String field) {

            }

            @Override
            public String getId() {
                return UUID.randomUUID().toString();
            }

            @Override
            public String getType() {
                return "payment";
            }

            @Override
            public PaymentDTO getPayment() {
                return new PaymentDTO();
            }

            @Override
            public LocalTime getCreation() {
                return LocalTime.now().plusMinutes(1);
            }
        };
    }
}
