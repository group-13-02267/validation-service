package unittests.validation.receivers;

import DTO.MerchantValidateDTO;
import DTO.PaymentDTO;
import DTO.TokenValidateDTO;
import core.AsyncService;
import messaging.MessageSender;
import org.junit.Before;
import org.junit.Test;
import validation.receivers.RefundValidatorService;
import validation.repository.ValidationRepository;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class RefundValidatorServiceTest {
    private ValidationRepository repository;

    private MessageSender sender;

    private AsyncService<PaymentDTO> service;

    @Before
    public void setup() {
        repository = mock(ValidationRepository.class);
        sender = mock(MessageSender.class);
        service = new RefundValidatorService(repository, sender);
    }

    @Test
    public void receive_whenReceivingPayment_shouldSendValidateMessages() {
        PaymentDTO payment = new PaymentDTO();
        payment.id = UUID.randomUUID().toString();
        payment.sender = "sender";
        payment.receiver = "receiver";
        payment.token = UUID.randomUUID().toString();
        payment.amount = 100F;

        service.receive(payment);

        verify(repository).createValidation(payment.id, "refund", payment);
        verify(sender).sendMessage(any(MerchantValidateDTO.class), eq("validate.merchant"));
        verify(sender).sendMessage(any(TokenValidateDTO.class), eq("validate.token"));
    }
}
