package unittests.validation.receivers;

import DTO.PaymentDTO;
import DTO.PaymentResponseDTO;
import DTO.ValidationDTO;
import core.AsyncService;
import messaging.MessageSender;
import org.junit.Before;
import org.junit.Test;
import validation.receivers.MerchantValidateService;
import validation.repository.DTUValidation;
import validation.repository.ValidationRepository;

import java.util.UUID;

import static org.mockito.Mockito.*;

public class MerchantValidateServiceTest {

    private ValidationRepository repository;

    private MessageSender sender;

    private AsyncService<ValidationDTO> validateService;

    @Before
    public void setup() {
        repository = mock(ValidationRepository.class);
        sender = mock(MessageSender.class);
        validateService = new MerchantValidateService(repository, sender);
    }

    @Test
    public void receive_whenReceivingMerchantValidationRequest_shouldValidateMessage() throws Exception {
        ValidationDTO request = new ValidationDTO();
        request.id = UUID.randomUUID().toString();
        request.status = true;

        PaymentDTO paymentDTO = new PaymentDTO();
        paymentDTO.id = request.id;
        paymentDTO.token = UUID.randomUUID().toString();
        paymentDTO.receiver = "receiver";
        paymentDTO.sender = "sender";
        paymentDTO.amount = 100F;

        when(repository.getValidation(request.id)).thenReturn(new DTUValidation(request.id, "payment", paymentDTO));
        when(repository.isValid(request.id)).thenReturn(true);

        validateService.receive(request);

        verify(repository).setValidation(request.id, request.status, "merchant");
        verify(sender).sendMessage(any(PaymentResponseDTO.class), eq("transaction.payment.validated." + request.id));
        verify(repository).deleteValidation(request.id);
    }

    @Test
    public void receive_whenReceivingRefundMerchantValidationRequest_shouldValidateAndSendMessage() throws Exception {
        validateService = new MerchantValidateService(repository, sender);
        ValidationDTO request = new ValidationDTO();
        request.id = UUID.randomUUID().toString();
        request.status = true;

        PaymentDTO paymentDTO = new PaymentDTO();
        paymentDTO.id = request.id;
        paymentDTO.token = UUID.randomUUID().toString();
        paymentDTO.receiver = "receiver";
        paymentDTO.sender = "sender";
        paymentDTO.amount = 100F;

        when(repository.getValidation(request.id)).thenReturn(new DTUValidation(request.id, "refund", paymentDTO));
        when(repository.isValid(request.id)).thenReturn(true);

        validateService.receive(request);

        verify(repository).setValidation(request.id, request.status, "merchant");
        verify(sender).sendMessage(any(PaymentResponseDTO.class), eq("transaction.refund.validated." + request.id));
        verify(repository).deleteValidation(request.id);
    }
}
