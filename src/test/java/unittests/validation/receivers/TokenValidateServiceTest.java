package unittests.validation.receivers;

import DTO.PaymentDTO;
import DTO.PaymentResponseDTO;
import DTO.TokenValidationDTO;
import core.AsyncService;
import messaging.MessageSender;
import org.junit.Before;
import org.junit.Test;
import validation.receivers.TokenValidateService;
import validation.repository.DTUValidation;
import validation.repository.ValidationRepository;

import java.util.UUID;

import static org.mockito.Mockito.*;

public class TokenValidateServiceTest {
    private ValidationRepository repository;

    private MessageSender sender;

    private AsyncService<TokenValidationDTO> validateService;

    @Before
    public void setup() {
        repository = mock(ValidationRepository.class);
        sender = mock(MessageSender.class);
    }

    @Test
    public void receive_whenReceivingMerchantValidationRequest_shouldValidateMessage() throws Exception {
        validateService = new TokenValidateService(repository, sender);
        TokenValidationDTO request = new TokenValidationDTO();
        request.id = UUID.randomUUID().toString();
        request.status = true;
        request.customer = "customer-id";

        PaymentDTO payment = new PaymentDTO();
        payment.id = request.id;
        payment.token = "token";
        payment.amount = 100F;
        payment.sender = request.customer;
        payment.receiver = "merchant";

        when(repository.getValidation(request.id)).thenReturn(new DTUValidation(request.id, "payment", payment));
        when(repository.isValid(request.id)).thenReturn(true);

        validateService.receive(request);

        verify(repository).setValidation(request.id, request.status, "token");
        verify(sender).sendMessage(any(PaymentResponseDTO.class), eq("transaction.payment.validated." + request.id));
    }

    @Test
    public void receive_whenReceivingRefundMerchantValidationRequest_shouldValidateAndSendMessage() throws Exception {
        validateService = new TokenValidateService(repository, sender);
        TokenValidationDTO request = new TokenValidationDTO();
        request.id = UUID.randomUUID().toString();
        request.status = true;
        request.customer = "customer-id";

        PaymentDTO payment = new PaymentDTO();
        payment.id = request.id;
        payment.token = "token";
        payment.amount = 100F;
        payment.sender = request.customer;
        payment.receiver = "merchant";

        when(repository.getValidation(request.id)).thenReturn(new DTUValidation(request.id, "refund", payment));
        when(repository.isValid(request.id)).thenReturn(true);

        validateService.receive(request);

        verify(repository).setValidation(request.id, request.status, "token");
        verify(sender).sendMessage(any(PaymentResponseDTO.class), eq("transaction.refund.validated." + request.id));
    }
}
