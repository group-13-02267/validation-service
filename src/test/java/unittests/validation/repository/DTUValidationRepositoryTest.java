package unittests.validation.repository;

import DTO.PaymentDTO;
import org.junit.Before;
import org.junit.Test;
import validation.repository.DTUValidationRepository;
import validation.repository.ValidationRepository;

import java.security.InvalidKeyException;

import static org.junit.Assert.*;

public class DTUValidationRepositoryTest {

    private ValidationRepository repository;

    @Before
    public void setup() {
        repository = new DTUValidationRepository();
    }

    @Test
    public void createValidation_whenCreatingValidation_shouldCreateAndStoreValidation() throws Exception {
        String id = "id";
        PaymentDTO payment = new PaymentDTO();
        repository.createValidation(id, "payment", payment);
        assertNotNull(repository.getPayment(id));
    }

    @Test(expected = InvalidKeyException.class)
    public void createValidation_whenGettingValidation_shouldThrowInvalid() throws Exception {
        String id = "id";
        assertNotNull(repository.getPayment(id));
    }

    @Test
    public void setValidation_whenSettingValidation_shouldBeValidated() throws Exception {
        String id = "id";
        PaymentDTO payment = new PaymentDTO();
        repository.createValidation(id, "payment", payment);
        repository.setValidation(id, true, "token");
        repository.setValidation(id, true, "merchant");
        assertTrue(repository.isValid(id));
    }

    @Test
    public void setValidation_whenSettingOnlyOneValidation_shouldBeNotValidated() throws Exception {
        String id = "id";
        PaymentDTO payment = new PaymentDTO();
        repository.createValidation(id, "payment", payment);
        repository.setValidation(id, true, "token");
        assertFalse(repository.isValid(id));
    }

    @Test
    public void getValidation_whenGettingPayment_shouldReturnPayment() throws Exception {
        String id = "id";
        PaymentDTO payment = new PaymentDTO();
        repository.createValidation(id, "payment", payment);
        assertEquals(payment, repository.getPayment(id));
    }

    @Test(expected = InvalidKeyException.class)
    public void getValidation_whenGettingNonExistingPayment_shouldThrowException() throws Exception {
        String id = "id";
        repository.getPayment(id);
    }

    @Test(expected = InvalidKeyException.class)
    public void deleteValidation_whenDeletingExistingPayment_shouldBeDeleted() throws Exception {
        String id = "id";
        PaymentDTO payment = new PaymentDTO();
        repository.createValidation(id, "payment", payment);
        repository.deleteValidation(id);
        repository.getPayment(id);
    }

    @Test
    public void getAllValidations_whenGettingAllValidations_shouldReturnListWithValidations() throws Exception {
        String id = "id";
        PaymentDTO payment = new PaymentDTO();
        repository.createValidation(id, "payment", payment);
        assertFalse(repository.getAllValidations().isEmpty());
    }
}
