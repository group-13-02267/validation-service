package servicetests;

import DTO.*;
import core.AsyncService;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.MessageSender;
import validation.receivers.MerchantValidateService;
import validation.receivers.PaymentValidatorService;
import validation.receivers.TokenValidateService;
import validation.repository.DTUValidationRepository;
import validation.repository.ValidationRepository;

import java.security.InvalidKeyException;
import java.util.UUID;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class InvalidatePaymentSteps {

    private ValidationRepository validationRepository;

    private MessageSender sender;

    private AsyncService<PaymentDTO> validatorService;

    private AsyncService<TokenValidationDTO> tokenValidatorService;

    private AsyncService<ValidationDTO> merchantValidatorService;

    private PaymentDTO paymentDTO;

    private ValidationDTO validationDTO;

    private TokenValidationDTO tokenValidationDTO;

    public InvalidatePaymentSteps() {
        sender = mock(MessageSender.class);
        validationRepository = new DTUValidationRepository();
        validatorService = new PaymentValidatorService(validationRepository, sender);
        tokenValidatorService = new TokenValidateService(validationRepository, sender);
        merchantValidatorService = new MerchantValidateService(validationRepository, sender);
    }

    @Given("^an invalid payment request$")
    public void anInvalidPaymentRequest() {
        paymentDTO = new PaymentDTO();
        paymentDTO.id = UUID.randomUUID().toString();
        paymentDTO.receiver = "merchant-id";
        paymentDTO.amount = 100F;
        paymentDTO.token = UUID.randomUUID().toString();
    }

    @When("^the invalid payment request is received$")
    public void theInvalidPaymentRequestIsReceived() {
        validatorService.receive(paymentDTO);
    }

    @Then("^the payment merchant will try to be evaluated by broadcasting \"([^\"]*)\"$")
    public void thePaymentMerchantWillTryToBeEvaluatedByBroadcasting(String arg0) throws Throwable {
        verify(sender).sendMessage(any(MerchantValidateDTO.class), eq("validate.merchant"));
    }

    @And("^the payment token will try to be validated by broadcasting \"([^\"]*)\"$")
    public void thePaymentTokenWillTryToBeValidatedByBroadcasting(String arg0) throws Throwable {
        verify(sender).sendMessage(any(TokenValidateDTO.class), eq("validate.token"));
    }

    @When("^the event `merchant\\.validate\\.\\*` has invalidated the merchant$")
    public void theEventMerchantValidateHasInvalidatedTheMerchant() {
        validationDTO = new ValidationDTO();
        validationDTO.id = paymentDTO.id;
        validationDTO.status = false;
        merchantValidatorService.receive(validationDTO);
    }

    @And("^the event `token\\.validate\\.\\*` has invalidated the token$")
    public void theEventTokenValidateHasInvalidatedTheToken() {
        tokenValidationDTO = new TokenValidationDTO();
        tokenValidationDTO.id = paymentDTO.id;
        tokenValidationDTO.customer = "customer-id";
        tokenValidationDTO.status = true;
        tokenValidatorService.receive(tokenValidationDTO);
    }

    @Then("^the invalidated payment token does not exist anymore$")
    public void thePaymentTokenIsNotValidated() {
        try {
            boolean isValid = validationRepository.isValid(paymentDTO.id);
            assertTrue(false);
        } catch (InvalidKeyException e) {
            assertTrue(true);
        }
    }
}
