package servicetests;

import DTO.*;
import core.AsyncService;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.MessageSender;
import validation.receivers.MerchantValidateService;
import validation.receivers.PaymentValidatorService;
import validation.receivers.TokenValidateService;
import validation.repository.DTUValidationRepository;
import validation.repository.ValidationRepository;

import java.security.InvalidKeyException;
import java.util.UUID;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ValidatePaymentSteps {

    private ValidationRepository validationRepository;

    private MessageSender sender;

    private AsyncService<PaymentDTO> validatorService;

    private AsyncService<TokenValidationDTO> tokenValidatorService;

    private AsyncService<ValidationDTO> merchantValidatorService;

    private PaymentDTO paymentDTO;

    private ValidationDTO validationDTO;

    private TokenValidationDTO tokenValidationDTO;

    public ValidatePaymentSteps() {
        sender = mock(MessageSender.class);
        validationRepository = new DTUValidationRepository();
        validatorService = new PaymentValidatorService(validationRepository, sender);
        tokenValidatorService = new TokenValidateService(validationRepository, sender);
        merchantValidatorService = new MerchantValidateService(validationRepository, sender);
    }

    @Given("^a payment request$")
    public void aPaymentRequest() {
        paymentDTO = new PaymentDTO();
        paymentDTO.id = UUID.randomUUID().toString();
        paymentDTO.receiver = "merchant-id";
        paymentDTO.amount = 100F;
        paymentDTO.token = UUID.randomUUID().toString();
    }

    @When("^the payment request is received$")
    public void thePaymentRequestIsReceived() {
        validatorService.receive(paymentDTO);
    }

    @Then("^the payment merchant is validated by broadcasting \"([^\"]*)\"$")
    public void theMerchantIsValidatedByBroadcasting(String arg0) throws Throwable {
        verify(sender).sendMessage(any(MerchantValidateDTO.class), eq("validate.merchant"));
    }

    @And("^the payment token is validated by broadcasting \"([^\"]*)\"$")
    public void theTokenIsValidatedByBroadcasting(String arg0) throws Throwable {
        verify(sender).sendMessage(any(TokenValidateDTO.class), eq("validate.token"));
    }

    @When("^the event `merchant\\.validate\\.\\*` for the payment is received$")
    public void theEventMerchantValidateIsReceived() {
        validationDTO = new ValidationDTO();
        validationDTO.id = paymentDTO.id;
        validationDTO.status = true;
        merchantValidatorService.receive(validationDTO);
    }

    @And("^the event `token\\.validate\\.\\*` for the payment is received$")
    public void theEventTokenValidateIsReceived() {
        tokenValidationDTO = new TokenValidationDTO();
        tokenValidationDTO.id = paymentDTO.id;
        tokenValidationDTO.customer = "customer-id";
        tokenValidationDTO.status = true;
        tokenValidatorService.receive(tokenValidationDTO);
    }

    @Then("^the payment token is validated$")
    public void theTokenIsValidated() {
        try {
            boolean isValid = validationRepository.isValid(paymentDTO.id);
            assertTrue(false);
        } catch (InvalidKeyException e) {
            assertTrue(true);
        }
    }

    @And("^The payment is validated by broadcasting `transaction\\.payment\\.validated\\.\\*`$")
    public void thePaymentIsValidatedByBroadcastingTransactionPaymentValidated() {
        verify(sender).sendMessage(any(PaymentResponseDTO.class), eq("transaction.payment.validated." + paymentDTO.id));
    }
}
