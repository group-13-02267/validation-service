package servicetests;

import DTO.*;
import core.AsyncService;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.MessageSender;
import validation.receivers.MerchantValidateService;
import validation.receivers.RefundValidatorService;
import validation.receivers.TokenValidateService;
import validation.repository.DTUValidationRepository;
import validation.repository.ValidationRepository;

import java.security.InvalidKeyException;
import java.util.UUID;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class ValidateRefundSteps {

    private ValidationRepository validationRepository;

    private MessageSender sender;

    private AsyncService<PaymentDTO> validatorService;

    private AsyncService<TokenValidationDTO> tokenValidatorService;

    private AsyncService<ValidationDTO> merchantValidatorService;

    private PaymentDTO paymentDTO;

    private ValidationDTO validationDTO;

    private TokenValidationDTO tokenValidationDTO;

    public ValidateRefundSteps() {
        sender = mock(MessageSender.class);
        validationRepository = new DTUValidationRepository();
        validatorService = new RefundValidatorService(validationRepository, sender);
        tokenValidatorService = new TokenValidateService(validationRepository, sender);
        merchantValidatorService = new MerchantValidateService(validationRepository, sender);
    }

    @Given("a refund request")
    public void aRefundRequest() {
        paymentDTO = new PaymentDTO();
        paymentDTO.id = UUID.randomUUID().toString();
        paymentDTO.sender = "merchant-id";
        paymentDTO.amount = 100F;
        paymentDTO.token = UUID.randomUUID().toString();
    }

    @When("the refund request is received")
    public void theRefundRequestIsReceived() {
        validatorService.receive(paymentDTO);
    }

    @Then("the merchant is validated by broadcasting {string}")
    public void theMerchantIsValidatedByBroadcasting(String arg0) {
        verify(sender).sendMessage(any(MerchantValidateDTO.class), eq("validate.merchant"));
    }

    @And("the token is validated by broadcasting {string}")
    public void theTokenIsValidatedByBroadcasting(String arg0) {
        verify(sender).sendMessage(any(TokenValidateDTO.class), eq("validate.token"));
    }

    @When("the event `merchant.validate.*` is received")
    public void theEventMerchantValidateIsReceived() {
        validationDTO = new ValidationDTO();
        validationDTO.id = paymentDTO.id;
        validationDTO.status = true;
        merchantValidatorService.receive(validationDTO);
    }

    @And("the event `token.validate.*` is received")
    public void theEventTokenValidateIsReceived() {
        tokenValidationDTO = new TokenValidationDTO();
        tokenValidationDTO.id = paymentDTO.id;
        tokenValidationDTO.customer = "customer-id";
        tokenValidationDTO.status = true;
        tokenValidatorService.receive(tokenValidationDTO);
    }

    @Then("the token is validated")
    public void theTokenIsValidated() {
        try {
            boolean isValid = validationRepository.isValid(paymentDTO.id);
            assertTrue(false);
        } catch (InvalidKeyException e) {
            assertTrue(true);
        }
    }

    @And("The refund is validated by broadcasting `transaction.refund.validated.*`")
    public void theRefundIsValidatedByBroadcastingTransactionRefundValidated() {
        verify(sender).sendMessage(any(PaymentResponseDTO.class), eq("transaction.refund.validated." + paymentDTO.id));
    }
}
