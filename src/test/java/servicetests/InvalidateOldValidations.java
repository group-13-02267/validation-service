package servicetests;

import DTO.PaymentDTO;
import DTO.PaymentResponseDTO;
import invalidation.DTUInvalidator;
import invalidation.Invalidator;
import invalidation.InvalidatorTimerTask;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.MessageSender;
import validation.repository.DTUValidationRepository;
import validation.repository.ValidationRepository;

import java.security.InvalidKeyException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class InvalidateOldValidations {

    private final MessageSender sender;

    private final ValidationRepository repository;

    private final Invalidator invalidator;

    private final TimerTask timerTask;

    private final Timer timer;

    private String id;

    public InvalidateOldValidations() {
        sender = mock(MessageSender.class);
        repository = new DTUValidationRepository();
        invalidator = new DTUInvalidator(repository);
        timerTask = new InvalidatorTimerTask(sender, invalidator, 0);
        timer = new Timer();
    }

    @Given("^a validation$")
    public void aValidation() {
        id = UUID.randomUUID().toString();
        repository.createValidation(id, "payment", new PaymentDTO());
        try {
            repository.getValidation(id);
        } catch (Exception e) {
            assertTrue(false);
        }
    }

    @When("^validation expires$")
    public void validationExpires() throws InterruptedException {
        timer.scheduleAtFixedRate(timerTask, 0, 5);
        Thread.sleep(100);
    }

    @Then("^should be deleted$")
    public void shouldBeDeleted() {
        try {
            repository.getValidation(id);
            assertTrue(false);
        } catch (InvalidKeyException e) {
            assertTrue(true);
        }
    }

    @And("^broadcast payment handled once per expired validation$")
    public void broadcastPaymentHandled() {
        verify(sender, atMostOnce())
                .sendMessage(any(PaymentResponseDTO.class), eq("transaction.payment.handled." + id));
    }
}
