Feature: Validate Payment

  Scenario: Validating An Incoming Payment
    Given a payment request
    When the payment request is received
    Then the payment merchant is validated by broadcasting "validate.merchant"
    And the payment token is validated by broadcasting "validate.token"
    When the event `merchant.validate.*` for the payment is received
    And the event `token.validate.*` for the payment is received
    Then the payment token is validated
    And The payment is validated by broadcasting `transaction.payment.validated.*`

