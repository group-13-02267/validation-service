Feature: Expired Validation

  Scenario: A Validation Expires
    Given a validation
    When validation expires
    Then should be deleted
    And broadcast payment handled once per expired validation

