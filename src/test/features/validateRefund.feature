Feature: Validate Refund

  Scenario: Validating An Incoming Refund
    Given a refund request
    When the refund request is received
    Then the merchant is validated by broadcasting "validate.merchant"
    And the token is validated by broadcasting "validate.token"
    When the event `merchant.validate.*` is received
    And the event `token.validate.*` is received
    Then the token is validated
    And The refund is validated by broadcasting `transaction.refund.validated.*`

