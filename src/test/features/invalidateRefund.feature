Feature: Invalidate Refund

  Scenario: Invalidating An Incoming Refund
    Given an invalid refund request
    When the invalid refund request is received
    Then the merchant will try to be evaluated by broadcasting "validate.merchant"
    And the token will try to be validated by broadcasting "validate.token"
    When the event `merchant.validate.*` has invalidated merchant
    And the event `token.validate.*` has invalidated token
    Then the token is not validated

