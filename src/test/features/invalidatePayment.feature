Feature: Invalidate Payment

  Scenario: Invalidating An Incoming Payment
    Given an invalid payment request
    When the invalid payment request is received
    Then the payment merchant will try to be evaluated by broadcasting "validate.merchant"
    And the payment token will try to be validated by broadcasting "validate.token"
    When the event `merchant.validate.*` has invalidated the merchant
    And the event `token.validate.*` has invalidated the token
    Then the invalidated payment token does not exist anymore

